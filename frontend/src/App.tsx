import React from 'react';
import logo from './logo.svg';
import './App.css';
import { NoteTrainer } from './components/NoteTrainer/NoteTrainer';

function App() {
  return (
    <div className="App">
      <NoteTrainer />
    </div>
  );
}

export default App;
